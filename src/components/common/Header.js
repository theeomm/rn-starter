import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

const Header = ({ headerTitle }) => (
        <View style={container}>
            <Text style={headerText}>{headerTitle}</Text>
        </View>
)
// setup styles
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#f8f8f8',
        height: 60,
        justifyContent: 'center',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    headerText: {
        fontSize: 24,

    }
})

// Destruct styles
const { container, headerText } = styles

export default Header