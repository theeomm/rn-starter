import React, { Component } from 'react'

//Import custom components
import Header from './components/common/Header'

const APP_NAME = 'RN Starter'
export default class App extends Component {
    render(){
        return (
            <Header headerTitle={APP_NAME} /> 
        )
    }
}